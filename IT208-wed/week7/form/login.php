<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <div>
        <div>
            <h1>User Login Form</h1>
        </div>
        <form action="user_login.php" method="post">
            <table>
                <tr>
                    <td><label for="user_name">User Name</label></td>
                    <td>:</td>
                    <td><input type="text" name="user_name"/></td>
                </tr>
                <tr>
                    <td><label for="password">Password</label></td>
                    <td>:</td>
                    <td><input type="password" name="password"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input style="float:right;" type="submit" value="Login"/></td>
                </td>
            </table>
            
        </form>
    </div>
</body>
</html>